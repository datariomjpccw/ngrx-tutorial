# NgrxTutorial

Tutorial from this blog: https://blog.imaginea.com/ngrx-introduction-and-its-basic-setup-with-angular/

## Dependencies

npm install -g json-server
npm install @fortawesome/fontawesome-free @ngrx/store@7 @ngrx/effects@7 @ngrx/store-devtools@7 --save
npm install concurrently --save-dev

## Project structure files

- ng g c components/customers-list -m app
- ng g c components/customers-list/customers-table -m app
- ng g c components/customer-details -m app
- ng g s services/customer -m app
- ng g cl models/customer --type=model --skipTests=true
- ng g i models/appState --type=state --skipTests=true
- mv src/app/models/app-state.state.ts src/app/models/app.state.ts
- mkdir src/app/store
- touch src/app/store/action.types.ts
- touch src/app/store/customer.actions.ts
- touch src/app/store/customer.effect.ts
- touch src/app/store/customer.reducer.ts

## Development

- npm start
