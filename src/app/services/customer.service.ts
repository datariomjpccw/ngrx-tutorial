import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { catchError, tap } from 'rxjs/operators';

import * as CustomerActions from '../store/customer.actions';
import { Customer } from '../models/customer.model';
import { AppState } from '../models/app.state';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class CustomerService {
  private baseUrl = 'http://localhost:3000/customers';
  constructor(
    private http: HttpClient,
    private store: Store<AppState>,
  ) { }

  /**
   * Get customers list
   */
  getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.baseUrl)
      .pipe(
        tap(customers => customers),
        catchError(this.handleError('getCustomers', []))
      );
  }

  /**
   * deleteCustomer
   */
  deleteCustomer(customer: Customer | number): Observable<Customer> {
    const id = typeof customer === 'number' ? customer : customer.id;
    const url = `${ this.baseUrl }/${ id }`;
    return this.http.delete<Customer>(url, httpOptions)
      .pipe(
        tap(() => {
          console.log(`deleted customer id=${ id }`);
          this.store.dispatch(new CustomerActions.loadCustomersAction());
        }),
        catchError(this.handleError<Customer>('deleteCustomer'))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${ operation } failed: ${ error.message }`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
