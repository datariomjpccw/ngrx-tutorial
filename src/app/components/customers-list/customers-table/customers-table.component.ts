import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from '../../../models/customer.model';

@Component({
  selector: 'app-customers-table',
  templateUrl: './customers-table.component.html',
  styleUrls: ['./customers-table.component.scss']
})
export class CustomersTableComponent {
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'emailAddress'];
  @Input() customers: Customer[];
  @Output() delete = new EventEmitter<Customer>();

  deleteCustomer(customer: Customer) {
    if (customer && customer.id) {
      if (confirm(`Are you sure want to delete customer ${ customer.firstName }`)) {
        this.delete.emit(customer);
      }
    }
  }
}
