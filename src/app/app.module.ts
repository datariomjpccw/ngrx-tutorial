import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


import { AppComponent } from './app.component';
import { CustomerReducer } from './store/customer.reducer';
import { CustomerEffects } from './store/customer.effect';
import { environment } from 'src/environments/environment';
import { CustomersListComponent } from './components/customers-list/customers-list.component';
import { CustomersTableComponent } from './components/customers-list/customers-table/customers-table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    CustomersListComponent,
    CustomersTableComponent,
  ],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    HttpClientModule,
    StoreModule.forRoot({ applicationState: CustomerReducer }),
    EffectsModule.forRoot([CustomerEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
